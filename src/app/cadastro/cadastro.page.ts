import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  private nome: string;
  private idade: string;
  private email: string;
  private senha: string;
  private endereco: string;

  constructor(private http: HttpClient) { }


  cadastro(){
    const body = {
      address: this.endereco,
      age: this.idade,
      email: this.email,
      name: this.nome,
      userPassword: this.senha
    }

    this.http.post('https://example-ecommerce.herokuapp.com/user/customer/add', body, {responseType: "text"}).subscribe(
      (resultado: any) => {
        console.log(resultado);
      },(erro) => {
        console.log(erro);
      }
      )
    
  }

  ngOnInit() {
  }

}
