import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.page.html',
  styleUrls: ['./listar.page.scss'],
})
export class ListarPage implements OnInit {

  public produtos:any;
  constructor( public navCtrl: NavController, public http: HttpClient) { 
    this.listarProdutos();
  }

  listarProdutos(){
    let data:Observable<any>;
    data = this.http.get('https://example-ecommerce.herokuapp.com/product/list');
    data.subscribe(resultado => {
      this.produtos = resultado;
    })
  }

  ngOnInit() {
  }

}
