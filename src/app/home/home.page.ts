import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ListarPage } from '../listar/listar.page';
import { UrlTree, Router } from '@angular/router';
import { Armazenamento } from '../domain/armazenamento'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router : Router, private storage: Armazenamento) {
    // if(this.storage.resgatarToken){
    //   this.router.navigate(['listar'])
    // }
  }

  irLogin(): void{
    this.router.navigate(['login'])
  }
  irCadastro(): void{
    this.router.navigate(['cadastro'])
  }
  irLista(): void{
    this.router.navigate(['listar'])
  }

  
}
