

import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class Armazenamento {
    constructor(private storage: Storage) {}

    public salvarToken(key: string, valor: object){
        this.storage.set(key, valor);
    }

    public resgatarToken(key: string){
        this.storage.get(key)
            .then((token) =>{
                return token;
            })
            .catch((err)=> {
                console.log("Ocorreu um erro: " + err);
            })
    }

}